package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyCareerObject"                                
     , summary=""
     , relativeUrl=""
     , connection="Salesforce"
     )             
public class MyCareerObject {

	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='Careers']")
	public WebElement Career;
	@LinkType()
	@FindBy(linkText = "Current Openings")
	public WebElement currentOpenings;
	@TextType()
	@FindBy(id = "MainContent_txtKeyword")
	public WebElement enterDesignationRoleEtc;
	@LinkType()
	@FindBy(linkText = "Technical Consultant")
	public WebElement technicalConsultant;
			
}
